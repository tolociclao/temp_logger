#include <Arduino.h>
#include <Wire.h>
#include <SD.h>
#include <RTClib.h>
#include <DHT.h>
#include <LowPower.h>

#define cspin 9
#define DHT_PIN 5
#define INTERRUPT_PIN 2

#define dirName String(now.year())

RTC_DS3231 rtc;
File logFile;
DHT dht(DHT_PIN, DHT22);


String month[12] = {"ene", "feb", "mar", "abr", "may", "jun", "jul", "ago", "sep", "oct", "nov", "dic"};
String fileName = "reg_";

const int N_READINGS = 100;

float getTemp(){

   int _N_READINGS = N_READINGS;
   float _summation = 0;
   float _temp[N_READINGS];

   for(int _n = 0; _n <= (N_READINGS - 1); _n++){
      _temp[_n] =  dht.readTemperature();
      if(isnan(_temp[_n])){
         _N_READINGS--;
      }else{_summation = _summation + _temp[_n];}
      delay(600);
   }

   return _summation/N_READINGS;

}

float getHum(){

   int _N_READINGS = N_READINGS;
   float _summation = 0;
   float _hum[N_READINGS];

   for(int _n = 0; _n <= (N_READINGS - 1); _n++){
      _hum[_n] =  dht.readHumidity();
      if(isnan(_hum[_n])){
         _N_READINGS--;
      }else{_summation = _summation + _hum[_n];}
      delay(600);
   }

   return _summation/N_READINGS;

}

void writeLine(String _col_1, String _col_2, String _col_3){
   //Serial.println("Escribiendo " + _col_1 + ',' +  _col_2 + ',' + _col_3 + "...");
   logFile.print(_col_1);
   logFile.print(',');
   logFile.print(_col_2);
   logFile.print(',');
   logFile.println(_col_3);
   //Serial.println("Escritura relizada");
}

void writeLog(DateTime _now, String _dirName, String _fileName){
   
   String _file_path  = '/' + _dirName + '/' + _fileName + month[_now.month() - 1] + ".csv";

   String _timeStamp = _now.timestamp(); //ISO_8601

   String _temp = String(getTemp());
   String _hum = String(getHum());

   while(!SD.begin(cspin)){// Serial.println("Insertar tarjeta SD");
   }

   if(SD.exists(_dirName)){

      if(SD.exists(_file_path)){      

         //Serial.println("Abriendo " + _file_path + "...");

         logFile = SD.open(_file_path, FILE_WRITE);
         if(logFile){

            writeLine(_timeStamp, _temp, _hum);
           // Serial.println("Cerrando...");
            logFile.close();

         }else{
            //Serial.println("Error abriendo " + _file_path);
         }

      }else{

         logFile = SD.open(_file_path, FILE_WRITE);
         //Serial.println("Abriendo " + _file_path + "...");

         if(logFile){

            writeLine("TimeStamp", "Temperatura[ºC]", "Humedad Relativa");
            writeLine(_timeStamp, _temp, _hum);
            logFile.close();
           // Serial.println("Cerrando...");

         }else{
            //Serial.println("Error abriendo " + _file_path);
         }
      }

   }else{

     // Serial.println("Creando el directorio " + _dirName + "...");
      SD.mkdir(_dirName);
     // Serial.println("Abriendo" + _file_path + "...");
      logFile = SD.open(_file_path, FILE_WRITE);

      if(logFile){

         writeLine("TimeStamp", "Temperatura[ºC]", "Humedad Relativa");
         writeLine(_timeStamp, _temp, _hum);
         logFile.close();
         //Serial.println("Cerrando...");
         
      }else{
         //Serial.println("Error abriendo " + _file_path);
      }
   }
}

void wakeUp(){

}
