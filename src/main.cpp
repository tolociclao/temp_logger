//For a better understanding of RTC based alarms: 
//https://github.com/garrysblog/DS3231-Alarm-With-Adafruit-RTClib-Library/blob/master/DS3231-RTClib-Adafruit-Alarm-Poll-SQW/DS3231-RTClib-Adafruit-Alarm-Poll-SQW.ino
//https://garrysblog.com/2020/07/05/using-the-ds3231-real-time-clock-alarm-with-the-adafruit-rtclib-library/


#include "temp_logger.h"

void setup(){

   Serial.begin(9600);

   pinMode(INTERRUPT_PIN, INPUT);

   dht.begin();
   rtc.begin();
   
   //rtc.adjust(DateTime(F(__DATE__),F( __TIME__)));


   rtc.disableAlarm(1);
   rtc.disableAlarm(2);
   rtc.clearAlarm(1);
   rtc.clearAlarm(2);

   rtc.writeSqwPinMode(DS3231_OFF); // Place SQW pin into alarm interrupt mode
   //DateTime now = rtc.now();
   rtc.setAlarm1(DateTime(0, 0, 0, 0, 0, 0), DS3231_A1_Minute);

}

void loop(){

   attachInterrupt(digitalPinToInterrupt(INTERRUPT_PIN), wakeUp, FALLING);

   delay(100);

   LowPower.powerDown(SLEEP_FOREVER, ADC_OFF, BOD_OFF);

   detachInterrupt(digitalPinToInterrupt(INTERRUPT_PIN)); 


   DateTime now = rtc.now();

   writeLog(now,dirName,fileName);

   rtc.disableAlarm(1);
   rtc.clearAlarm(1);
   rtc.setAlarm1(DateTime(0, 0, 0, 0, 0, 0), DS3231_A1_Minute);

} 